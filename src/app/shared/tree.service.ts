import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

import { Element } from '../tree/element.interface';

@Injectable()
export class TreeService {
  private elementsTree = new Subject<Element[]>();
  private currentStateOfTree: Element[];
  public getTree$;

  constructor() {
    this.getTree$ = this.elementsTree.asObservable();
  }

  updateTree(newTree) {
    this.elementsTree.next(newTree);
    this.currentStateOfTree = newTree;
  }

  getCurrentTree() {
    return this.currentStateOfTree;
  }

}
