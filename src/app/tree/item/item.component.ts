import { Component, OnInit, Input } from '@angular/core';
import { TreeService } from '../../shared/tree.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent implements OnInit {
  @Input('element') currentElement;

  public editedTitle = '';

  public isAddFormOpen = false;
  public isEditFormOpen = false;

  public newItem = {
      id: '',
      title: '',
      level: null,
      children: [],
      isOpen: false
  };

  constructor(private treeService: TreeService) { }

  ngOnInit() {
  }

  showElement(id) {
    let currentTree = this.treeService.getCurrentTree();
    let itemsList = this.convertTreeToList(currentTree);
    let changedItemsList = itemsList.map(item => {
      if (item.id === id) item.isOpen = true;
      return item;
    });
    this.treeService.updateTree(this.convertListToTree(changedItemsList));
  }

  hideElement(id) {
    let currentTree = this.treeService.getCurrentTree();
    let itemsList = this.convertTreeToList(currentTree);
    let changedItemsList = itemsList.map(item => {
      if (item.id === id) item.isOpen = false;
      return item;
    });
    this.treeService.updateTree(this.convertListToTree(changedItemsList));
  }

  showAddForm() {
    this.isAddFormOpen = true;
    this.isEditFormOpen = false;
  }

  showEditForm() {
    this.isAddFormOpen = false;
    this.isEditFormOpen = true;
    this.editedTitle = this.currentElement.title;
  }

  add(parentId) {
    if (this.newItem.title.length == 0) return false;
    let currentTree = this.treeService.getCurrentTree();
    let itemsList = this.convertTreeToList(currentTree);
      itemsList.forEach(item => {
      if (item.id === parentId) {
        this.newItem.id = item.id + '-' + item.children.length;
        this.newItem.level = item.level + 1;
      }
    });
      itemsList.push(this.newItem);

    this.treeService.updateTree(this.convertListToTree(itemsList));
    this.newItem = {
        id: '',
        title: '',
        level: null,
        children: [],
        isOpen: false
    };
    this.isAddFormOpen = false;
  }

  edit(id) {
    if (this.editedTitle.length == 0) return false;
    let currentTree = this.treeService.getCurrentTree();
    let itemsList = this.convertTreeToList(currentTree);
    let changedItemsList = itemsList.map(item => {
      if (item.id === id) item.title = this.editedTitle;
      return item;
    });
    this.treeService.updateTree(this.convertListToTree(changedItemsList));
    this.editedTitle = '';
    this.isEditFormOpen = false;
  }

  remove(id) {
    let currentTree = this.treeService.getCurrentTree();
    let itemsList = this.convertTreeToList(currentTree);
    let changedItemsList = [];
      itemsList.forEach(item => {
      if (item.id !== id) changedItemsList.push(item);
    });
    this.treeService.updateTree(this.convertListToTree(changedItemsList));
  }

  convertTreeToList(tree) {
    let itemsList = [];
      tree.forEach(item => {
          itemsList.push(item);
      if (item.children.length > 0) {
        this.convertTreeToList(item.children).forEach(child => itemsList.push(child));
      }
    });

    return itemsList;
  }

  convertListToTree(itemsList) {
    let tree = [];
    let maxLevel = 0;
      itemsList.map(item => {
      if (item.level > maxLevel) maxLevel = item.level;
          item.children = [];
      return item;
    });

    for (let i = maxLevel; i >= 0; i--) {
      if (i == 0) {
          itemsList.forEach(item => {
          if(item.level == 0) tree.push(item);
        });
      } else {
          itemsList.forEach(item => {
          if (item.level === i) {
            let idParts = item.id.split('-');
            idParts.pop();
            let parentId = idParts.join('-');

              itemsList = itemsList.map(parent => {
              if (parent.id === parentId) parent.children.push(item);
              return parent;
            });
          }
        });
      }
    }
    return tree;
  }
}
