import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Observable } from 'rxjs/Rx';

import { ItemComponent } from './item.component';
import { TreeService } from '../../shared/tree.service';

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ItemComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [
          TreeService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    component.currentElement = {
      id: '0',
      title: 'Test element',
      level: 0,
      children: [],
      isOpen: false
    };
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should be showed adding form', () => {
    component.showAddForm();
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.new-element')).toBeTruthy();
  });

  it('should be showed editing form', () => {
    component.showEditForm();
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.edit-element')).toBeTruthy();
  });

  it('convertTreeToList() should convert tree to list', () => {
    let mockTree = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 1,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      }
    ];

    let targetList = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 1,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      },
      {
        id: '0-0',
        title: 'Sub test 1',
        level: 1,
        children: [],
        isOpen: false
      }
    ];
    let list = component.convertTreeToList(mockTree);
    expect(list).toEqual(targetList);
  });

  it('convertListToTree() should convert list to tree', () => {
    let mockList = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 1,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      },
      {
        id: '0-0',
        title: 'Sub test 1',
        level: 1,
        children: [],
        isOpen: false
      }
    ];

    let targetTree = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 1,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      }
    ];

    let tree = component.convertListToTree(mockList);
    expect(tree).toEqual(targetTree);
  });
});
