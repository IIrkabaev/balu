import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Element } from './element.interface';
import { TreeService } from '../shared/tree.service';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.sass']
})
export class TreeComponent implements OnInit {
  public tree: Element[];
  public treeProcessed = [];

  treeSubscription: Subscription;

  constructor(private treeService: TreeService) { }

  ngOnInit() {
    if (localStorage.getItem('tree') !== null) {
      this.tree = JSON.parse(localStorage.getItem('tree'));
      this.convertTreeToList(this.tree);
    } else {
      this.getDefaultTree();
    }

    this.treeService.updateTree(this.tree);

    this.treeSubscription = this.treeService.getTree$.subscribe(tree => {
      this.treeProcessed = [];
      this.tree = tree;
      this.saveCurrentState();
      this.convertTreeToList(tree);
    });
  }

  convertTreeToList(tree) {
      tree.forEach(element => {
      this.treeProcessed.push(element);
      if (element.children.length > 0 && element.isOpen) {
        this.convertTreeToList(element.children);
      }
    });
  }

  saveCurrentState() {
    localStorage.setItem('tree', JSON.stringify(this.tree));
  }

  getDefaultTree() {
      this.tree = [
          {
              id: '0',
              title: 'Element #1',
              level: 0,
              children: [],
              isOpen: false
          },
          {
              id: '1',
              title: 'Element #2',
              level: 0,
              children: [],
              isOpen: false
          },
          {
              id: '2',
              title: 'Element #3',
              level: 0,
              children: [
                  {
                      id: '2-0',
                      title: 'Child #1 of element #3',
                      level: 1,
                      children: [
                          {
                              id: '2-0-0',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-0-1',
                              title: 'Another subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-0-2',
                              title: 'Another subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-0-3',
                              title: 'Last subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          }
                      ],
                      isOpen: true
                  },
                  {
                      id: '2-1',
                      title: 'Child #2 of element #3',
                      level: 1,
                      children: [
                          {
                              id: '2-1-0',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-1-1',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-1-2',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          }
                      ],
                      isOpen: true
                  },
                  {
                      id: '2-2',
                      title: 'Child #3 of element #3',
                      level: 1,
                      children: [
                          {
                              id: '2-2-0',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          },
                          {
                              id: '2-2-1',
                              title: 'Subchild',
                              level: 2,
                              children: [],
                              isOpen: false
                          }
                      ],
                      isOpen: true
                  }
              ],
              isOpen: true
          }
      ];
      this.convertTreeToList(this.tree);
  }
}
