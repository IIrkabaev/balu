import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { TreeComponent } from './tree.component';
import { ItemComponent } from './item/item.component';
import { TreeService } from '../shared/tree.service';

describe('TreeComponent', () => {
  let component: TreeComponent;
  let fixture: ComponentFixture<TreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TreeComponent,
        ItemComponent
      ],
      imports: [
        FormsModule
      ],
      providers: [
          TreeService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should have default tree', () => {
    let tree = component.tree;
    expect(tree).toBeTruthy();
  });

  it('should have default objects list', () => {
    let list = component.treeProcessed;
    expect(list).toBeTruthy();
  });

  it('convertTreeToList() should works', () => {
    let mockTree = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 0,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      }
    ];
    component.treeProcessed = [];
    component.convertTreeToList(mockTree);
    let testedList = component.treeProcessed;
    let targetList = [
      {
        id: '0',
        title: 'Test 1',
        level: 0,
        children: [
          {
            id: '0-0',
            title: 'Sub test 1',
            level: 0,
            children: [],
            isOpen: false
          }
        ],
        isOpen: true
      },
      {
        id: '0-0',
        title: 'Sub test 1',
        level: 0,
        children: [],
        isOpen: false
      }
    ];
    expect(testedList).toEqual(targetList);
  });

  it('changes should be saved in localStorage', () => {
    let itemFixture = TestBed.createComponent(ItemComponent);
    let itemComponent = itemFixture.componentInstance;

    localStorage.removeItem('tree');

    itemComponent.currentElement = {
      id: '0',
      title: 'Test elem',
      level: 0,
      children: [],
      isOpen: false
    };

    itemComponent.newItem = {
      id: '',
      title: 'Test item',
      level: null,
      children: [],
      isOpen: false
    };

    itemComponent.add(0);
    itemFixture.detectChanges();

    expect(localStorage.getItem('tree')).toBeTruthy();
  });
});
