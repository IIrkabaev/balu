export interface Element {
  id: string,
  title: string,
  level: number,
  children?: Element[],
  isOpen: boolean
}
