import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TreeService } from './shared/tree.service';

import { AppComponent } from './app.component';

import { TreeComponent } from './tree/tree.component';
import { ItemComponent } from './tree/item/item.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
  ],
  declarations: [
      AppComponent,
      TreeComponent,
      ItemComponent
  ],
  providers: [
      TreeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
